import { useState } from 'react'
import Login from './Login'
import Registration from './Registration'

const AccessForm = () => {

    const [showLogin, setShowLogin] = useState(true)

    return (
        <div className="access-window">
            <div className="access-wrapper">
                <div className="buttons">
                    <button
                        onClick={() => setShowLogin(true)}
                        style={{ 
                            backgroundColor: showLogin ? 'white' : '#6E74A9',
                            color: showLogin ? '#6E74A9' : 'white',
                            boxShadow: showLogin ? 'none' : 'inset -3px 3px 6px rgb(0,0,0,.3)'
                             }}>
                        Old pal? Sign In ☕
                    </button>
                    
                    <button
                        onClick={() => setShowLogin(false)}
                        style={{
                            backgroundColor: showLogin ? '#6E74A9' : 'white',
                            color: showLogin ? 'white' : '#6E74A9',
                            boxShadow: showLogin ? 'inset -3px 3px 6px rgb(0,0,0,.3)' : 'none'
                             }}>
                        Newcomer? Sign Up ✔️
                    </button>
                </div>

                <div className="show-forms">
                    {
                        showLogin
                        ? <Login />
                        : <Registration />
                    }
                </div>
            </div>
        </div>
    )
}

export default AccessForm
