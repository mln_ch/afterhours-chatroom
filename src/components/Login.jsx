import { useState } from 'react'
import axios from 'axios'

const Login = () => {

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')

    const login = async (e) => {
        e.preventDefault()

        // authenticate
        const pId = 'f34f3220-9672-43ff-b113-9afd14bfd043'
        const authObject = { 'Project-ID': pId, 'User-Name': username, 'User-Secret': password }

        try {
            await axios.get('https://api.chatengine.io/chats', { headers: authObject })

            localStorage.setItem('userlog', username)
            localStorage.setItem('passlog', password)

            window.location.reload()
            setError('')

        } catch (error) {
            setError('...whomst you? 🤔')
        }
    }

    return (
        <div className="login-wrapper">
        <h1> Log in </h1>
        <h2 align="center" >{ error } </h2>

        <form className="singUp" onSubmit={login} >
            <input
                type="text"
                placeholder="ChatAdmin"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                required />

            <input type="password"
                    placeholder="chatadmin"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required />

            <div align="center" >
                <button
                    type="submit"
                    className="enter-chat-btn">
                        Start Chatting
                </button>
            </div>
        </form>
        
    </div>
    )
}

export default Login
