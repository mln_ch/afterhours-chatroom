import axios from 'axios'
import { useState } from 'react'

const Registration = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [newMail, setNewMail] = useState('')
    const [newFirst, setNewFirst] = useState('')
    const [newLast, setNewLast] = useState('')
    const [newAvatar, setNewAvatar] = useState(null)

    const register = async (e) => {
        e.preventDefault()

        const authObject = {
            'User-Name': username,
            'User-Secret': password,
            'first_name': newFirst,
            'last_name': newLast,
            'email': newMail,
            'avatar': newAvatar
        }
        
        try {
            await axios.get('https://api.chatengine.io/users/', {
                method: 'POST',
                headers: {
                    'PRIVATE-KEY': 'f34f3220-9672-43ff-b113-9afd14bfd043'
                },
                data: authObject })

            localStorage.setItem('userlog', username)
            localStorage.setItem('passlog', password)

            setUsername('')
            setPassword('')
            setNewMail('')
            setNewFirst('')
            setNewLast('')

            window.location.reload()

        } catch(error) {
            console.log(error)
        }


        // push new user to db.json
    }

    return (
        <div className="registration-wrapper">
            <h1> Register </h1>

            <form className="singUp" onSubmit={register} >
                <div className="row">
                    <input
                        type="text"
                        value={username}
                        placeholder='Pick an username'
                        onChange={ (e) => setUsername(e.target.value) }
                        className="user"
                        required />
                    
                    <input
                        type="password"
                        value={password}
                        placeholder='Pick a password'
                        onChange={ (e) => setPassword(e.target.value) }
                        className="secret"
                        required />
                </div>

                <div className="row">
                    <input
                        type="text"
                        value={newFirst}
                        placeholder="First Name"
                        onChange={ (e) => setNewFirst(e.target.value) }
                        className="first_name"
                        required />

                    <input
                        type="text"
                        value={newLast}
                        placeholder="Last Name"
                        onChange={ (e) => setNewLast(e.target.value) }
                        className="last_name"
                        required />
                </div>
                
                <div className="row">
                    <input
                        type="text"
                        value={newMail}
                        placeholder="Your eMail"
                        onChange={ (e) => setNewMail(e.target.value) }
                        className="email"
                        required />

                    <input
                        type="file"
                        multiple={false}
                        className="avatar"
                        onChange={(e) => setNewAvatar(e.target.files) } />
                </div>

                <div align="center">
                    <button
                        type="submit"
                        className="enter-chat-btn" >
                            Create Account
                    </button>
                </div>
            </form>
        </div>
    )
}

export default Registration
