import { useState } from 'react'
import { ChatEngine, getOrCreateChat } from 'react-chat-engine'
import './main.scss'
import ChatFeed from './components/ChatFeed'
import AccessForm from './components/AccessForm'

const pId = 'f34f3220-9672-43ff-b113-9afd14bfd043'

const App = () => {

    const [ uName, setUName ] = useState('')
    function createDirectChat(creds) {
        getOrCreateChat(
            creds,
            { is_direct_chat: true, usernames: [uName] },
            () => setUName('') 
        )
    }

    function renderChatForm(creds) {
        return (
            <div className="sendDm" >
                <input
                    placeholder="Send a DM to someone..."
                    value = { uName }
                    onChange= {(e) => setUName(e.target.value)}/>
                
                <button onClick={() => createDirectChat(creds)}>
                    Send
                </button>
            </div>
        )
    }

    if(!localStorage.getItem('userlog')) return <AccessForm />

    return (
        <ChatEngine
            height="100vh"
            projectID={ pId }
            userName={localStorage.getItem('userlog')}
            userSecret={localStorage.getItem('passlog')}

            renderChatFeed={ (chatAppProps) => <ChatFeed { ...chatAppProps } /> }
            renderNewChatForm={ (creds) => renderChatForm(creds) } />
    )
}

export default App
